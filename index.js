(function () {
    const whichIsLarger = (fun1, fun2) => {
        if (fun1 < fun2) {
            return 'f';
        } else if (fun1 > fun2) {
            return 'g';
        } else {
            return 'neither';
        }
    };
})();
